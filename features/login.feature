#language:pt

Funcionalidade: Login
    Para que eu possa cadastrar e gerenciar minhas tarefas
    Sendo um usuário
    Posso acessar o sistema com meu email e senha previamente cadastrados

    Contexto: Página principal
        Dado que eu acesso à página principal

    @logout
    Cenário: Usuário deve ser autorizado

        Quando eu faço login com "eu@papito.io" e "123456"
        Então devo ver o email "eu@papito.io" no dashboard
        E devo ver a seguinte mensagem "Olá, Fernando"
    
    Esquema do Cenário:

        Quando eu faço login com "<email>" e "<senha>"
        Então devo ver a seguinte mensagem "<alerta>"

        Exemplos:
        |Nº_EX|email|senha|alerta|
        |1|eu@papito.io|xpto123|Senha inválida.|
        |2|eu@papito.net|xpto123|Usuário não cadastrado.|
        |3|eupapito.io|xpto123|Email incorreto ou ausente.|
        |4||xpto123|Email incorreto ou ausente.|
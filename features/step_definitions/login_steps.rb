

Dado("que eu acesso à página principal") do
    visit 'https://mark7.herokuapp.com'
end
  
Quando('eu faço login com {string} e {string}') do |email, senha|
    @email = email
    LoginPage.new.faz_login(email, senha)    
end
  
Então("devo ver o email {string} no dashboard") do |string|
    expect(page).to have_content @email
end
  
Então('devo ver a seguinte mensagem {string}') do |mensagem|
    expect(page).to have_content mensagem
end